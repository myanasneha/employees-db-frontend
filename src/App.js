// import logo from './logo.svg';
import React from 'react';
import EmployeesListComponent from './components/EmployeesListComponent';
import './App.css';

function App() {
  return (
    <div className="wrapper">
      <EmployeesListComponent />
    </div>
  );
}

export default App;
