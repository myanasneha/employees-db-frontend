import React from 'react';
import { connect } from "react-redux";
import '../styles/design.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

class EmployeesListComponent extends React.PureComponent {
  constructor(props) {
    super();
    this.state = {
      empID: '',
      empName: '',
      empdob: new Date(),
      empDesignation: '',
      department: '',
      joiningDate: new Date(),
      gender: '',
      experience: '',
      country: '',
      employeesList: [],
      employeeDataErr: '',
      heading: 'Add',
      deleteSuccessMsg: ''
    };
    this.handleEmployeesList = this.handleEmployeesList.bind(this);
    this.handleAddEmployee = this.handleAddEmployee.bind(this);
    this.handleEditEmp = this.handleEditEmp.bind(this);
    this.handleDeleteEmp = this.handleDeleteEmp.bind(this);
    this.handleEditRecord = this.handleEditRecord.bind(this);
  }

  componentDidMount() {
    this.handleEmployeesList();
  }

  handleEmployeesList() {
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };
    fetch('/employees/list', requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      this.props.GetEmployeesList(data);
      this.setState({ employeesList: data && data.employeesList })
    })
  }

  handleAddEmployee() {
    let employeeData = {};
    employeeData['empID'] = new Date().valueOf();
    employeeData['empName'] = this.state.empName;
    employeeData['empdob'] = moment(this.state.empdob).format("MMM DD, YYYY");
    employeeData['empDesignation'] = this.state.empDesignation;
    employeeData['department'] = this.state.department;
    employeeData['joiningDate'] = moment(this.state.joiningDate).format("MMM DD, YYYY");
    employeeData['gender'] = this.state.gender;
    employeeData['experience'] = this.state.experience;
    employeeData['country'] = this.state.country;
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(employeeData)
    };
    fetch('/add/employee', requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      this.props.UpdateEmployeesListAfterAdd(data.resObj);
      this.setState({ 
        employeeDataErr: '',
        empName: '',
        empdob: new Date(),
        empDesignation: '',
        department: '',
        joiningDate: new Date(),
        gender: '',
        experience: '',
        country: '',
        employeesList: [...this.state.employeesList, data.resObj]
      });
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
  }

  handleEditRecord(item) {
    this.setState({
      heading: 'Edit',
      empID: item.empID,
      empName: item.empName,
      empdob: new Date(item.empdob),
      empDesignation: item.empDesignation,
      department: item.department,
      joiningDate: new Date(item.joiningDate),
      gender: item.gender,
      experience: item.experience,
      country: item.country
    });
  }

  handleEditEmp() {
    let employeeData = {};
    employeeData['empID'] = this.state.empID;
    employeeData['empName'] = this.state.empName;
    employeeData['empdob'] = moment(this.state.empdob).format("MMM DD, YYYY");
    employeeData['empDesignation'] = this.state.empDesignation;
    employeeData['department'] = this.state.department;
    employeeData['joiningDate'] = moment(this.state.joiningDate).format("MMM DD, YYYY");
    employeeData['gender'] = this.state.gender;
    employeeData['experience'] = this.state.experience;
    employeeData['country'] = this.state.country;
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(employeeData)
    };
    fetch(`/edit/employee/${this.state.empID}`, requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      this.props.UpdateEmployeesListAfterEdit(data.resObj);
      this.setState({ 
        employeeDataErr: '',
        empID: '',
        empName: '',
        empName: '',
        empdob: new Date(),
        empDesignation: '',
        department: '',
        joiningDate: new Date(),
        gender: '',
        experience: '',
        country: '',
        heading: 'Add'
      });
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
  }

  handleDeleteEmp(item) {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
    };
    fetch(`/delete/employee/${item.empID}`, requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      this.props.DeleteEmployee(data, item.empID);
      this.setState({
        deleteSuccessMsg: 'Successfully deleted'
      });
    })
    .catch(error => {
      console.error('There was an error!', error);
    });
  }

  render() {
    let employeesList = this.props.employeesList || []
    return (
      <div>
        <form>
          <fieldset>
            <h2>{this.state.heading == 'Add' ? 'Add' : 'Edit'} Employee Record</h2>
            <label>
              <p>Name: </p>
              <input type="text" value={this.state.empName} onChange={(e) => this.setState({ empName: e.target.value })} />
            </label>
            <label>
              <p>DoB: </p>
              <DatePicker selected={this.state.empdob} onChange={(date) => this.setState({ empdob: date })} />
            </label>
            <label>
              <p>Designation: </p>
              <input type="text" value={this.state.empDesignation} onChange={(e) => this.setState({ empDesignation: e.target.value })} step="1"/>
            </label>
            <label>
              <p>Department: </p>
              <input type="text" value={this.state.department} onChange={(e) => this.setState({ department: e.target.value })} step="1"/>
            </label>
            <label>
              <p>Joining Date: </p>
              <DatePicker selected={this.state.joiningDate} onChange={(date) => this.setState({ joiningDate: date })} />
            </label>
            <label>
              <p>Gender: </p>
              <select value={this.state.gender} onChange={(e) => this.setState({ gender: e.target.value })}>
                <option value="">Select Gender</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
            </label>
            <label>
              <p>Experience: </p>
              <input type="number" value={this.state.experience} onChange={(e) => this.setState({ experience: e.target.value })} step="1"/>
            </label>
            <label>
              <p>Country: </p>
              <input type="text" value={this.state.country} onChange={(e) => this.setState({ country: e.target.value })} step="1"/>
            </label>
            <p style={{color: 'red'}}>{this.state.employeeDataErr}</p>
            <button type="button" onClick={this.state.heading == 'Add' ? this.handleAddEmployee : this.handleEditEmp}>Submit</button>
          </fieldset>
        </form>
        <form>
          <fieldset>
            <h2>Employees List</h2>
            <table>
              <tr>
                <th>Emp ID</th>
                <th>Name</th>
                <th>DoB</th>
                <th>Designation</th>
                <th>Department</th>
                <th>Joining Date</th>
                <th>Gender</th>
                <th>Experience</th>
                <th>Country</th>
                <th>Actions</th>
              </tr>
              {employeesList && employeesList.length > 0 && employeesList.map((item, index) => 
                <tr key={index}>
                  <td>{item.empID}</td>
                  <td>{item.empName}</td>
                  <td>{item.empdob}</td>
                  <td>{item.empDesignation}</td>
                  <td>{item.department}</td>
                  <td>{item.joiningDate}</td>
                  <td>{item.gender}</td>
                  <td>{item.experience}</td>
                  <td>{item.country}</td>
                  <td>
                    <div className="btn-group"><span>
                      <button type="button" onClick={() => this.handleEditRecord(item)}>Edit</button>
                      <button type="button" onClick={() => this.handleDeleteEmp(item)}>Delete</button>
                    </span></div>
                  </td>
                </tr>
              )}
              {employeesList && employeesList.length < 1 &&
                <tr><td colSpan="10">No data found</td></tr>
              }
            </table>
          </fieldset>
        </form>
      </div>
    )
  }
}

const mapStatetoProps = state => {
  return {
    employeesList: state.employeesList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GetEmployeesList: (data) => dispatch({ type: "Employees_List", data: data }),
    UpdateEmployeesListAfterAdd: (data) => dispatch({ type: "Update_Employees_List_After_Add", data: data }),
    UpdateEmployeesListAfterEdit: (data) => dispatch({ type: "Update_Employees_List_After_Edit", data: data }),
    DeleteEmployee: (data, empID) => dispatch({ type: "Delete_Employee", data: data, empID: empID })
  };
};
export default connect(mapStatetoProps, mapDispatchToProps)(EmployeesListComponent);