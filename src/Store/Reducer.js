const initialState = {
  employeesCount: 0,
  employeesList: []
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };
  switch(action.type){
    
    case 'Employees_List':
      if(action.data){
        newState.employeesList = action.data.employeesList;
        newState.employeesCount = action.data.employeesCount;
      }
      break;

    case 'Update_Employees_List_After_Add':
      let updatedEmployeesList = newState.employeesList;
      updatedEmployeesList.push(action.data);
      newState.employeesList = updatedEmployeesList;
      newState.employeesCount = updatedEmployeesList.length;
      break;

    case 'Update_Employees_List_After_Edit':
      if(action.data) {
        let index = newState.employeesList.findIndex(emp => emp._id == action.data._id)
        newState.employeesList[index].empID = action.data.empID;
        newState.employeesList[index].empName = action.data.empName;
        newState.employeesList[index].empdob = action.data.empdob;
        newState.employeesList[index].empDesignation = action.data.empDesignation;
        newState.employeesList[index].department = action.data.department;
        newState.employeesList[index].joiningDate = action.data.joiningDate;
        newState.employeesList[index].gender = action.data.gender;
        newState.employeesList[index].experience = action.data.experience;
        newState.employeesList[index].country = action.data.country;
      }
      break;

    case 'Delete_Employee':
      if(action.data && action.data.resObj){
        let deleteIndex = newState.employeesList.findIndex(emp => emp.empID == action.empID)
        if(deleteIndex >= 0){
          newState.employeesList.splice(deleteIndex, 1);
          newState.employeesCount--
        }
      }
      break;

    default:
      break;
  }
  return newState;
};

export default reducer;